# MiFood - Partie Backend (Strapi) 

## Setup

```bash
# Installation de dépendances
$ yarn install

# Server tourne sur le localhost:4001
$ yarn develop

```

## Déploiement
### Avec Heroku
Pour le futur déploiement avec l'utilisation d'Heroku
[Doc Strapi - Heroku](https://strapi.io/documentation/3.0.0-beta.x/deployment/heroku.html). 
[Doc Heroku](https://devcenter.heroku.com/). 

## Doc Strapi
Pour plus de détails sur l'installation [Strapi docs](https://strapi.io/documentation/v3.x/getting-started/introduction.html).

