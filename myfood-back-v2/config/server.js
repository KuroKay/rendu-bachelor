module.exports = ({ env }) => ({
  "host": "0.0.0.0",
  "port": 4001,
  admin: {
    auth: {
      secret: env('ADMIN_JWT_SECRET', 'dc6f627b7527d37c6975cc731e2b6c6b'),
    },
  },
});
