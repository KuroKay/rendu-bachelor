# MiFood - Travail de Bachelor

Dans le cadre d'un travail d'étude, MiFood est un site de recette de cuisine qui à pour but de promouvoir l'agriculture locale.

## 3 niveaux (architecture 3-tiers) :

- La couche de présentation / Frontend
  - Réalisée avec Nuxt.js + Apollo + GraphQl

- La couche fonctionnelle / Backend 
  - Réalisée avec Strapi + GraphQl

-  La couche d’accès aux données / Database
   - En Sqlite

## Setup
Cloner le repertoire sur votre ordinateur. 
Vérifier que vous avez bien Yarn d'installer.

**Suivre les indications (Readme) se trouvant dans chaque dossier pour la suite de l'installation**

