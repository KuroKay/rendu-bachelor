import Strapi from 'strapi-sdk-javascript/build/main'

const apiUrl = process.env.API_URL || 'http://localhost:4001'
const strapi = new Strapi(apiUrl)

export default strapi;
export { apiUrl }