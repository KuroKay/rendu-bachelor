
export default {
  /*
  ** Nuxt rendering mode
  ** See https://nuxtjs.org/api/configuration-mode
  */
  mode: 'universal',
  /*
  ** Nuxt target
  ** See https://nuxtjs.org/api/configuration-target
  */
  target: 'server',
  /*
  ** Headers of the page
  ** See https://nuxtjs.org/api/configuration-head
  */
  head: {
    title: 'MiFood',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    script: [
      {
        src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'
      }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;700;800&family=Roboto:wght@300;400;500;700&display=swap' }
    ]
  },
  /*
  ** Global CSS
  */
 css: [
    "uikit/dist/css/uikit.min.css",
    "uikit/dist/css/uikit.css",
    "@/assets/scss/main.scss",
    'aos/dist/aos.css'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: '~/plugins/uikit.js', ssr: false },
    { src: '~/plugins/VueFlickity.js', ssr: false },
    { src: '~plugins/slide.js', ssr: false },
    { src: '~plugins/aos.js', ssr: false },
    { src: '~plugins/select.js', ssr: false },
    { src: '~plugins/modal.js', ssr: false }
  ],
  /*
  ** Auto import components
  ** See https://nuxtjs.org/api/configuration-components
  */
  components: true,
  /*
  ** Nuxt.js dev-modules
  */
 buildModules: [
   '@nuxtjs/fontawesome'
 ],
 fontawesome: {
    icons: {
      solid: ['faUserPlus','faUser','faSignOutAlt', 'faCheck', 'faHourglassHalf', 'faTemperatureHigh','faCog', 'faHeart', 'faUsers', 'faBook', 'faEdit', 'faShareAlt', 'faArrowRight', 'faBars' ]
    }
  },
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/apollo',
    '@nuxtjs/pwa',
  ],

  pwa: {
    manifest: {
      name: 'MyFood',
      short_name: 'Myfood',
      lang: 'fr',
      useWebmanifestExtension: false
    }
  },

  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  apollo: {
    clientConfigs: {
      default: {
        httpEndpoint: 'http://localhost:4001/graphql'
      }
    }
  },
  axios: {},
  /*
  ** Build configuration
  ** See https://nuxtjs.org/api/configuration-build/
  */
  build: {
  }
}
