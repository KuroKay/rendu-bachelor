# MiFood - Partie Frontend 

## Setup

```bash
# Installation de dépendances
$ yarn install

# Server tourne sur le localhost:3000
$ yarn dev

# build pour la production
$ yarn build
$ yarn start

```

## Déploiement
### Avec Heroku
Pour le futur déploiement avec l'utilisation [Heroku](https://nuxtjs.org/faq/heroku-deployment/). Préférer cette option si vous utilisez Strapi notament.

### Avec Netlify
Possibilité de le faire avec [Netlify](https://nuxtjs.org/faq/netlify-deployment).


## Doc Nuxt.js
Pour plus de détails sur l'installation [Nuxt.js docs](https://nuxtjs.org).

